module Admin

class AddPackagesController < ApplicationController
  before_action :set_add_package, only: [ :update, :destroy]

 before_filter :authenticate_user!

    layout 'admin'

###############################################################################################################################
#######################################################################################################################################
    def index
      @all_packages = AddPackage.paginate(:page => params[:page], :per_page => 10).order('position ASC')

      puts @all_packages.inspect
    end



    def show_posts
      @all_packages = AddPackage.where(:page_id => params[:id]).paginate(:page => params[:page], :per_page => 10).order('position ASC')
     # @page = AddPackage.find(params[:page_id])
    end
    


    def new
      @all_packages = AddPackage.new
    end
    


    def edit
      @all_packages = AddPackage.find(params[:id])

      puts @all_packages.inspect
    end
    


    def create
      @all_packages = AddPackage.new(page_params)
     
      if @all_packages.save
        flash[:notice] = "Package was created."
        redirect_to :action => 'index'
      else
        render :action => 'new'
      end
    end

  

    def update

      puts params[:id].inspect
      @all_packages = AddPackage.find(params[:id])

      if @all_packages.update(page_params)
        flash[:notice] = 'Package was successfully updated.'
        redirect_to :back
      else
        render action: 'edit'
      end
    end


    def publish
      page = AddPackage.find(params[:page_id])
      page.published = ( page.published == true ? false : true)
      page.save

      respond_to do |format|
        # format.html 
        format.js { list_refresh }

      end
    end


    def move_up
      page = AddPackage.find(params[:page_id])
      page.move_higher
      page.save

      respond_to do |format|
        # format.html 
        format.js { list_refresh }
      end
    end


    def move_down
      page =AddPackage.find(params[:page_id])
      page.move_lower
      page.save

      respond_to do |format|
        # format.html 
        format.js { list_refresh }
      end
    end

    def destroy
      @all_packages = AddPackage.find(params[:page_id])

      puts @all_packages.inspect

      @all_packages.destroy
     

      respond_to do |format|
        # format.html 
        format.js { list_refresh }
        puts  format.inspect

      end

    end




    private

      def list_refresh
        @pages = AddPackage.paginate(:page => params[:page], :per_page => 10).order('position ASC')
        
        return render(:file => 'admin/add_packages/list_refresh.js.erb')
      end


       # Use callbacks to share common setup or constraints between actions.
      def set_add_package
       # @parent_item = AddPackage.find(params[:page_id])
      end


      def page_params
        params.require(:add_package).permit(
          :placename, 
          :only_for_logged_in_members, 
          :myimage, 
          :days, 
          :nights, 
          :packagename, 
          :position, 
          :category,
          :price,
          :created_on, 
          :updated_on,
          :myimages_attributes => [:id, :photo, :headline, :caption, :done, :_destroy]
        )
      end
    
  end

end

