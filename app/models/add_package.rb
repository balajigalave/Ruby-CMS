class AddPackage < ActiveRecord::Base


acts_as_list
	validates :placename, :presence => true, :uniqueness => true
  validates :price, :presence => true
   has_many :images

  accepts_nested_attributes_for :images, :reject_if => :all_blank, :allow_destroy => true

	has_attached_file :myimage, :styles => { :normal => "800x800>", :thumb => "250x250#" }, :default_url => "http://placehold.it/500x500"
  validates_attachment_content_type :myimage, :content_type => /\Aimage\/.*\Z/

  def label
  	"AddPackages"
  end

  def form_title
  	name.downcase.gsub(' ', '_')
  end

end
