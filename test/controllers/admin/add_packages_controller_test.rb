require 'test_helper'

class Admin::AddPackagesControllerTest < ActionController::TestCase
  setup do
    @admin_add_package = admin_add_packages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_add_packages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_add_package" do
    assert_difference('Admin::AddPackage.count') do
      post :create, admin_add_package: {  }
    end

    assert_redirected_to admin_add_package_path(assigns(:admin_add_package))
  end

  test "should show admin_add_package" do
    get :show, id: @admin_add_package
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_add_package
    assert_response :success
  end

  test "should update admin_add_package" do
    patch :update, id: @admin_add_package, admin_add_package: {  }
    assert_redirected_to admin_add_package_path(assigns(:admin_add_package))
  end

  test "should destroy admin_add_package" do
    assert_difference('Admin::AddPackage.count', -1) do
      delete :destroy, id: @admin_add_package
    end

    assert_redirected_to admin_add_packages_path
  end
end
