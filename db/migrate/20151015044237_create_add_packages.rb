class CreateAddPackages < ActiveRecord::Migration
  def change
    create_table :add_packages do |t|

      t.string :placename ,presence: true,length: { minimum: 4 }
      t.integer :days, presence: true 
      t.integer :nights, presence: true
      t.string :packagename,presence: true
      t.string :category, presence: true
      t.string :facility,presence: true
      t.integer :price, presence: true
      t.boolean :only_for_logged_in_members, :default => false
      t.attachment :myimage
      t.integer :position
      t.timestamps null: false
    end
    add_index :add_packages,:placename, unique: true
  end
end
